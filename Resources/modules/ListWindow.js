(function( module ){
	
	var ListWindow = function( data ){
		this.data = data || ( this.data = [] );
	
		this.init();
		return this;
	};

	ListWindow.prototype.init = function(){
		var self = this;
		// this sets the background color of the master UIView (when there are no windows/tab groups on it)
		Titanium.UI.setBackgroundColor('#000');
		
		// create tab group
		this.tabGroup = Titanium.UI.createTabGroup();
		
		
		//
		// create base UI tab and root window
		//
		this.win1 = Titanium.UI.createWindow({  
		    title:'Tab 1',
		    backgroundColor:'#fff'
		});
		this.tab1 = Titanium.UI.createTab({  
		    icon:'KS_nav_views.png',
		    title:'Tab 1',
		    window: this.win1
		});
		
		this.label1 = Titanium.UI.createLabel({
			color:'#999',
			text:'I am Window 1',
			font:{fontSize:20,fontFamily:'Helvetica Neue'},
			textAlign:'center',
			width:'auto'
		});
		
		this.win1.add( this.label1 );
		
		
		
		var data = [];
		
		this.data.forEach(function( item, i){
			data.push({ 
				title:item.label 
			});	
		});
		
		this.tableView = Ti.UI.createTableView({
			data: data
		});
		
		this.win1.add( this.tableView );
		
		this.tabGroup.addTab( this.tab1 );  
		//tabGroup.addTab(tab2);  
		
		
		// open tab group
		this.tabGroup.open();

	};
	
	module.exports = ListWindow;
})( module );
