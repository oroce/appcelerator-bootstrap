(function( module ){
	var Home = function(){
		this.init();
		return this;
	};
	
	
	var events = {
		label1Click: function(){
		
			var ListWindow = require( "/modules/ListWindow" );
			
			var t = new ListWindow([{
				"label": "01"
			},{
				"label": "02"
			},{
				"label": "03"
			},{
				"label": "04"
			},{
				"label": "05"
			},{
				"label": "06"
			}]);
		}
	};
	
	Home.prototype.delegateEvents = function(){															
		var self = this;
		this.label1.addEventListener( "click", function(){
			events.label1Click.apply( self, arguments ); 
		});
	};
	
	Home.prototype.init = function(){
		// this sets the background color of the master UIView (when there are no windows/tab groups on it)
		Titanium.UI.setBackgroundColor('#000');
		
		// create tab group
		this.tabGroup = Titanium.UI.createTabGroup();
		
		
		//
		// create base UI tab and root window
		//
		this.win1 = Titanium.UI.createWindow({  
		    title:'Tab 1',
		    backgroundColor:'#fff'
		});
		this.tab1 = Titanium.UI.createTab({  
		    icon:'KS_nav_views.png',
		    title:'Tab 1',
		    window: this.win1
		});
		
		this.label1 = Titanium.UI.createLabel({
			color:'#999',
			text:'I am Window 1',
			font:{fontSize:20,fontFamily:'Helvetica Neue'},
			textAlign:'center',
			width:'auto'
		});
		
		this.win1.add( this.label1 );
		
		
		this.tabGroup.addTab( this.tab1 );  
		//tabGroup.addTab(tab2);  
		
		
		// open tab group
		this.tabGroup.open();
		
		this.delegateEvents();
	};
	
	module.exports = Home;
})( module );
