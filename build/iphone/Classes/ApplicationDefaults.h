/**
 * Appcelerator_Bootstrap Appcelerator_Bootstrap_Bootstrap Mobile
 * Copyright (c) 2009-2011 by Appcelerator_Bootstrap, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 * 
 * WARNING: This is generated code. Modify at your own risk and without support.
 */

#import <Foundation/Foundation.h>


@interface ApplicationDefaults : NSObject {
	
}
+ (NSMutableDictionary *) copyDefaults;
@end