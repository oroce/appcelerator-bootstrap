/**
 * Appcelerator_Bootstrap Appcelerator_Bootstrap_Bootstrap Mobile
 * Copyright (c) 2009-2012 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 * 
 * WARNING: This is generated code. Modify at your own risk and without support.
 */
#ifdef USE_TI_UIIPHONESYSTEMBUTTON

#import "TiUIiPhoneSystemButtonProxy.h"

@implementation TiUIiPhoneSystemButtonProxy

MAKE_SYSTEM_PROP(ACTION,UIBarButtonSystemItemAction);
MAKE_SYSTEM_PROP(CAMERA,UIBarButtonSystemItemCamera);
MAKE_SYSTEM_PROP(COMPOSE,UIBarButtonSystemItemCompose);
MAKE_SYSTEM_PROP(BOOKMARKS,UIBarButtonSystemItemBookmarks);
MAKE_SYSTEM_PROP(SEARCH,UIBarButtonSystemItemSearch);
MAKE_SYSTEM_PROP(ADD,UIBarButtonSystemItemAdd);
MAKE_SYSTEM_PROP(TRASH,UIBarButtonSystemItemTrash);
MAKE_SYSTEM_PROP(REPLY,UIBarButtonSystemItemReply);
MAKE_SYSTEM_PROP(STOP,UIBarButtonSystemItemStop);
MAKE_SYSTEM_PROP(REFRESH,UIBarButtonSystemItemRefresh);
MAKE_SYSTEM_PROP(PLAY,UIBarButtonSystemItemPlay);
MAKE_SYSTEM_PROP(PAUSE,UIBarButtonSystemItemPause);
MAKE_SYSTEM_PROP(FAST_FORWARD,UIBarButtonSystemItemFastForward);
MAKE_SYSTEM_PROP(REWIND,UIBarButtonSystemItemRewind);
MAKE_SYSTEM_PROP(EDIT,UIBarButtonSystemItemEdit);
MAKE_SYSTEM_PROP(CANCEL,UIBarButtonSystemItemCancel);
MAKE_SYSTEM_PROP(SAVE,UIBarButtonSystemItemSave);
MAKE_SYSTEM_PROP(ORGANIZE,UIBarButtonSystemItemOrganize);
MAKE_SYSTEM_PROP(DONE,UIBarButtonSystemItemDone);
MAKE_SYSTEM_PROP(FLEXIBLE_SPACE,UIBarButtonSystemItemFlexibleSpace);
MAKE_SYSTEM_PROP(FIXED_SPACE,UIBarButtonSystemItemFixedSpace);

MAKE_SYSTEM_PROP(ACTIVITY,UIAppcelerator_Bootstrap_BootstrapNativeItemSpinner);
MAKE_SYSTEM_PROP(SPINNER,UIAppcelerator_Bootstrap_BootstrapNativeItemSpinner);
MAKE_SYSTEM_PROP(INFO_LIGHT,UIAppcelerator_Bootstrap_BootstrapNativeItemInfoLight);
MAKE_SYSTEM_PROP(INFO_DARK,UIAppcelerator_Bootstrap_BootstrapNativeItemInfoDark);
MAKE_SYSTEM_PROP(DISCLOSURE,UIAppcelerator_Bootstrap_BootstrapNativeItemDisclosure);
MAKE_SYSTEM_PROP(CONTACT_ADD,UIAppcelerator_Bootstrap_BootstrapNativeItemContactAdd);


@end

#endif